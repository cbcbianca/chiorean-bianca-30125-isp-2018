package Chiorean.Bianca.g30125.l9.e3;
//Create an application which let a user enter a file name in a text field and then when a 
//button is pressed to display the content of the file in a text area.
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import Chiorean.Bianca.g30125.l9.e1.ButtonAndTextFields2;



public class FileContentDisplay extends JFrame{
	 
    HashMap accounts = new HashMap();

    JLabel File;
    JTextField tFile;
    JTextField tArea;
    JButton Button;

    FileContentDisplay(){

         

          setTitle("Choose the file!");
          setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
          init();
          setSize(200,300);
          setVisible(true);
    }

    public void init(){

          this.setLayout(null);
          int width=80;int height = 20;

          File = new JLabel("FileName ");
          File.setBounds(10, 50, width, height);

          tFile = new JTextField();
          tFile.setBounds(70,50,width, height);

      

          Button = new JButton("Read");
          Button.setBounds(10,150,width, height);

          Button.addActionListener(new ButtonPressed());

          tArea = new JTextField();
          tArea.setBounds(10,180,150,80);

          add(File);add(tFile);add(Button);
          add(tArea);

    }
    
    public static void main(String[] args) {
        new FileContentDisplay();
    }
    
    class ButtonPressed implements ActionListener{
    	
    	@Override
        public void actionPerformed(ActionEvent e) {
        
    	BufferedReader br;
    	String fileName = FileContentDisplay.this.tFile.getText();
    	String fileContent="";
    	 try {
             Path path = Paths.get(fileName);
             if (Files.exists(path)){
                 br = new BufferedReader(new FileReader(fileName));
                 String line = br.readLine();
                 while (line != null){
                     fileContent=fileContent+"\n"+line;
                     line=br.readLine();
                 }
                 FileContentDisplay.this.tArea.setText(fileContent);
             } else{
                 FileContentDisplay.this.tArea.setText("File cannot be found.");
             }
         } catch (FileNotFoundException e1) {
             e1.printStackTrace();
         } catch (IOException e1) {
             e1.printStackTrace();
         }
     }
 }
}
