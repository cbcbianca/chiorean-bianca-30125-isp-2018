//Create an application which let to users to play X&0 game.

package Chiorean.Bianca.g30125.l9.e4;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.JOptionPane;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class XO extends JFrame {
    JButton[][] buttons=new JButton[3][3];
    static int delay=0;

    static int counter=1;
    XO()
    {   super();this.setLayout(null);
        setTitle("X si 0");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        initializare();
        setSize(400,400);
        setVisible(true);


    }
    void verificare()
    {boolean game_won=false;
      String winner = null;

        try{
            if(!game_won &&buttons[0][0].getText() == buttons[0][1].getText() &&
                    buttons[0][1].getText()==buttons[0][2].getText()  && buttons[0][0].getText()!="?") {
                game_won = true;
                winner=buttons[0][0].getText();
                buttons[0][0].setBackground(Color.red);
                buttons[0][1].setBackground(Color.red);
                buttons[0][2].setBackground(Color.red);
                delay++;


            }
            if(!game_won &&buttons[0][0].getText()==buttons[1][0].getText() &&
                    buttons[1][0].getText()==buttons[2][0].getText() && buttons[0][0].getText()!="?") {
                game_won = true; winner=buttons[0][0].getText();
                buttons[0][0].setBackground(Color.red);
                buttons[1][0].setBackground(Color.red);
                buttons[2][0].setBackground(Color.red);
                delay++;

            }
            if(!game_won &&buttons[0][0].getText()==buttons[1][1].getText() &&
                    buttons[1][1].getText()==buttons[2][2].getText() && buttons[0][0].getText()!="?") {
                game_won = true; winner=buttons[0][0].getText();
                buttons[0][0].setBackground(Color.red);
                buttons[1][1].setBackground(Color.red);
                buttons[2][2].setBackground(Color.red);
                delay++;

            }
            if(!game_won &&buttons[1][0].getText()==buttons[1][1].getText() &&
                    buttons[1][1].getText()==buttons[1][2].getText() && buttons[1][0].getText()!="?") {
                game_won = true; winner=buttons[1][1].getText();
                buttons[1][0].setBackground(Color.red);
                buttons[1][1].setBackground(Color.red);
                buttons[1][2].setBackground(Color.red);
                delay++;
            }

            if(!game_won &&buttons[0][1].getText()==buttons[1][1].getText() &&
                    buttons[1][1].getText()==buttons[2][1].getText() && buttons[2][1].getText()!="?")
            { winner=buttons[0][1].getText();
                game_won=true;
                buttons[0][1].setBackground(Color.red);
                buttons[1][1].setBackground(Color.red);
                buttons[2][1].setBackground(Color.red);
                delay++;
            }


            if( !game_won && buttons[2][0].getText()==buttons[1][2].getText() &&
                    buttons[1][2].getText()==buttons[0][2].getText() && buttons[2][0].getText()!="?")
            {   winner=buttons[2][0].getText();
                game_won=true;
                buttons[2][0].setBackground(Color.red);
                buttons[1][2].setBackground(Color.red);
                buttons[0][2].setBackground(Color.red);
                delay++;
            }
            if( !game_won && buttons[2][0].getText()==buttons[2][1].getText() &&
                    buttons[2][1].getText()==buttons[2][2].getText() && buttons[2][0].getText()!="?")
            {   winner=buttons[2][0].getText();
                game_won=true;
                buttons[2][0].setBackground(Color.red);
                buttons[2][1].setBackground(Color.red);
                buttons[2][2].setBackground(Color.red);
                delay++;
            }


        }catch (Exception e)
        {
            System.out.println("Nasoleala");
        }
        finally {
            if(delay==2)
            if(game_won==true && winner!="?") {
                System.out.println("Joc castigat de " + winner);
                try{
                    Thread.sleep(3500);
                    dispose();
                }
                catch (Exception e)
                {
                    System.out.println("-");
                }


            }
                
        }
    }
    void initializare()
    {
        JPanel joc=new JPanel(new GridLayout(3,3));
        int width=80,height=80;
        for(int i=0;i<3;i++)
            for(int j=0;j<3;j++) {
                buttons[i][j] = new JButton("?");
                buttons[i][j].setBounds(70+j*width,70+i*height,width,height);
                add(buttons[i][j]);
                buttons[i][j].addActionListener(new Apasare());
            }
    }
    class Apasare implements ActionListener {

        public void actionPerformed(ActionEvent e)
        {counter++;verificare();
            if(e.getSource()==buttons[0][0])
            {   if(counter %2==0)
                buttons[0][0].setText("X");
                else
                    buttons[0][0].setText("O");
                buttons[0][0].setEnabled(false);

            }
            if(e.getSource()==buttons[0][1])
            {   if(counter %2==0)
                buttons[0][1].setText("X");
            else
                buttons[0][1].setText("O");

            buttons[0][1].setEnabled(false);

            }
            if(e.getSource()==buttons[0][2])
            {   if(counter %2==0)
                buttons[0][2].setText("X");
            else
                buttons[0][2].setText("O");
                buttons[0][2].setEnabled(false);

            }
            if(e.getSource()==buttons[1][0])
            {   if(counter %2==0)
                buttons[1][0].setText("X");
            else
                buttons[1][0].setText("O");
                buttons[1][0].setEnabled(false);

            }
            if(e.getSource()==buttons[1][1])
            {   if(counter %2==0)
                buttons[1][1].setText("X");
            else
                buttons[1][1].setText("O");
                buttons[1][1].setEnabled(false);

            }
            if(e.getSource()==buttons[1][2])
            {   if(counter %2==0)
                buttons[1][2].setText("X");
            else
                buttons[1][2].setText("O");
                buttons[1][2].setEnabled(false);

            }
            if(e.getSource()==buttons[2][0])
            {   if(counter %2==0)
                buttons[2][0].setText("X");
            else
                buttons[2][0].setText("O");
                buttons[2][0].setEnabled(false);

            }
            if(e.getSource()==buttons[2][1])
            {   if(counter %2==0)
                buttons[2][1].setText("X");
            else
                buttons[2][1].setText("O");
                buttons[2][1].setEnabled(false);

            }
            if(e.getSource()==buttons[2][2])
            {   if(counter %2==0)
                buttons[2][2].setText("X");
            else
                buttons[2][2].setText("O");
                buttons[2][2].setEnabled(false);

            }


        }


    }

    public static void main(String[] args) {
        new XO();
    }

}

