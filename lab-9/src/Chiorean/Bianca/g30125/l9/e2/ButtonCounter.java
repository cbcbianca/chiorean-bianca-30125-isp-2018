package Chiorean.Bianca.g30125.l9.e2;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import Chiorean.Bianca.g30125.l9.e1.ButtonAndTextFields2;


public class ButtonCounter extends JFrame{
   
	HashMap accounts = new HashMap();
	 
    
    JTextField tArea;
    JButton Push;
    int counter=0;
    
    ButtonCounter(){
    	 
     

        setTitle("Push the button");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(200,300);
        setVisible(true);
  }
    
    public void init(){
    	 
        this.setLayout(null);
        int width=80;int height = 20;

        Push = new JButton("Push and Count");
        Push.setBounds(10,150,width, height);

        Push.addActionListener(new TratareButonPush());

        tArea = new JTextField();
        tArea.setBounds(10,180,150,80);

        add(Push);
        add(tArea);
        

  }
    
    public static void main(String[] args) {
        new ButtonCounter();
  }
    
    class TratareButonPush implements ActionListener{
    	
    	@Override
        public void actionPerformed(ActionEvent e) {
     
              
       
                    counter++;
                    ButtonCounter.this.tArea.setText(""+counter);
                   
       
                  
              }

        }    
  }

