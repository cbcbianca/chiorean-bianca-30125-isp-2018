package chiorean.bianca.g30125.l7.e2;


import java.util.Collections;

import java.util.ArrayList;


public class Bank {
	
	 ArrayList<BankAccount> bankaccount=new ArrayList<BankAccount>();
	 
	 public void addAccount(String owner,double balance)
	 {
		BankAccount b =new BankAccount(owner,balance);
		bankaccount.add(b);
	 }
	 
	 public void printAccounts()
	 {
		   
	  System.out.print(bankaccount.toString());
			
	 }
	 //implement a printAccounts with range which will display all accounts between min range and max range values.
	 
	 public void printAccounts(double minBalance, double maxBalance)
	 {
		for(BankAccount ba: bankaccount)
		{
			if(ba.getBalance()>minBalance)
			{
				if(ba.getBalance()<maxBalance)
				{
					System.out.println(ba.getOwner());
				}
			}
		}
	 }
	 
	 public BankAccount getAccount(String owner)
	 {
		for(BankAccount ba: bankaccount)
		{
			if(ba.getOwner()==owner)
			{
			return ba;
			}
		}
		return null;
	 }
	 
// get in test main method all bank accounts (using getAllAccounts() method) and sort them alphabetically by owner field.
	
	 public void getAllAccounts(){
		 Collections.sort(bankaccount);
		  
			    System.out.print(bankaccount.toString());
			 
	 }
}
