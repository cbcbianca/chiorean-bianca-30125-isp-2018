package chiorean.bianca.g30125.l7.e2;

import java.util.Comparator;


public class BankAccount implements Comparable <BankAccount>{
	 private String owner;
     private double balance;
     
     public BankAccount(String owner,double balance)
     {
    	this.owner=owner;
    	this.balance=balance;
     }
     
     public String getOwner() {
		return owner;
	}
     
	@Override
	public String toString() {
		return "BankAccount [owner=" + owner + ", balance=" + balance + "]";
	}

    public void setOwner(String owner) {
		this.owner = owner;
	}
    
    public double getBalance() {
		return balance;
	}

    public void setBalance(double balance) {
		this.balance = balance;
	}

    public void withdraw(double amount)
     {
    	 if (balance>amount)
    	 {
    		 balance=balance-amount;
    		 System.out.println("New balance:"+balance);
    	 }
    	 else System.out.println("Out of money");
     }
     
     public void deposit(double amount)
     {
    	 balance=balance+amount;
    	 System.out.println("New balance:"+balance);
     }
     
     @Override
     public int compareTo(BankAccount other) {

    		int CompareInt=this.owner.compareTo(other.owner);
            if (CompareInt<0) return -1;
            if(CompareInt>0) return 1;
            return 0;
       
    	 }
     }

   
