package chiorean.bianca.g30125.l7.e4;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class Dictionary {

	HashMap<Word,Definition> h = new HashMap<Word,Definition>();

	public void addWord(Word w,Definition d)
	{
		h.put(w, d); // cheia ii cuvantul si valoarea ii definitia
	}

	public Definition getDefinition(Word w)
	{
		if(h.containsKey(w)) {
			System.out.println("\n"+h.get(w).getDescription());
			return h.get(w);
		}
		System.out.println("Cuvantul cautat nu a fost gasit!");


		return null;
	}

	public void getAllWords()
	{
		Set a = h.entrySet();
		Iterator iterator= a.iterator();
		System.out.println("\n");
		while(iterator.hasNext())
		{
			Map.Entry<Word,Definition> intmap = (Map.Entry<Word,Definition>)iterator.next();
			System.out.println("\n"+intmap.getKey().getName());
		}
	}

	public void getAllDefinition()
	{
		Set a = h.entrySet();
		Iterator iterator= a.iterator();
		System.out.println("\n");
		while(iterator.hasNext())
		{
			Map.Entry<Word,Definition> intmap = (Map.Entry<Word,Definition>)iterator.next();
			System.out.println("\n"+intmap.getValue().getDescription());
		}

	}


}



