package chiorean.bianca.g30125.l7.e4;

public class Main {
	
	public static void main(String[] args) {
		Dictionary d = new Dictionary();
		Word cuv = new Word("cuv");
		Definition def = new Definition("def");		
		Word cuv1 = new Word("cuv1");
		Definition def1 = new Definition("def1");	
		Word cuv2 = new Word("cuv2");
		Definition def2 = new Definition("def2");	
		d.addWord(cuv,def);
		d.addWord(cuv1,def1);
		d.addWord(cuv2,def2);
		
		d.getAllDefinition();
		d.getDefinition(cuv);
		d.getAllWords();
	}
}
