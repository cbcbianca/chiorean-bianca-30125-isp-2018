package chiorean.bianca.g30125.l7.e4;

public class Word {
	
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Word(String name) {
		this.name = name;
	}
	

}