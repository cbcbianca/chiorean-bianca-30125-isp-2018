package chiorean.bianca.g30125.l7.e3;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Objects;

public class BankAccount implements Comparable<BankAccount> {
    private String owner;
    private int balance;
	
    public BankAccount(String owner, int balance)  {
	
		this.owner = owner;
		this.balance = balance;
	}
    
    public void withdraw(double amount)
    {
    	balance-=amount;
    }
    
    public void deposit(double amount)
    {
    	balance+=amount;
    }

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public int getBalance() {
		return balance;
	}

	public void setBalance(int balance) {
		this.balance = balance;
	}
    
	@Override
    public int compareTo(BankAccount o)
    {
        if(balance<o.getBalance())
            return -1;
        else
        {
            if(balance==o.getBalance())
                return 0;
            else
                return 1;
        }
    }
	
	@Override
    public int hashCode() {

        return Objects.hash(owner, balance);
    }
    
    
}
