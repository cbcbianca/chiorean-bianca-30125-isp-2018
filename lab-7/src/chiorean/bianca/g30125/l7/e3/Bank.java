package chiorean.bianca.g30125.l7.e3;
//Create a new version for the application from previous exercise (with the same functionality) 
//and replace ArrayList with a TreeSet.

import java.util.*;

public class Bank {
    private TreeSet <BankAccount> bankaccounts=new TreeSet<BankAccount>(); 
   
    public Bank()
    {
    	bankaccounts=new TreeSet<BankAccount>();
    }
    
    public Bank(TreeSet<BankAccount> bankaccounts) {
        this.bankaccounts = bankaccounts;
    }
    
    public void addAccount(String owner,int balance)
    {
    	bankaccounts.add(new BankAccount(owner,balance));	
    }
    
    public BankAccount getAccount(String owner)
    {
        Iterator<BankAccount> iterator=bankaccounts.iterator();
        BankAccount account;
        while(iterator.hasNext())
        {
            account=iterator.next();
            if(account.getOwner().equals(owner))
                return account;
        }
        return null;
    }
    
    public void printAccounts()
    {
    	Set<BankAccount> BankAccountSet = new TreeSet<BankAccount>();

        Iterator<BankAccount> iterator=bankaccounts.iterator();
        BankAccount account;
        while(iterator.hasNext())
        {
            account=iterator.next();
            System.out.println(account.getOwner() + " " + account.getBalance());
        }
    }
    
    public void printAccounts(double minBalance, double maxBalance) {
        Set<BankAccount> BankAccountSet = new TreeSet<BankAccount>();
        Iterator<BankAccount> iterator = bankaccounts.iterator();
        BankAccount bankAccount;
        while (iterator.hasNext()) {
            bankAccount = iterator.next();
            if (bankAccount.getBalance() <= maxBalance && bankAccount.getBalance() >= minBalance) 
            {
                System.out.println(bankAccount.getOwner() + " " + bankAccount.getBalance());

            }
        }
    }

    public TreeSet<BankAccount> getAllAccounts() {
        return bankaccounts;
    }
    
    
    
    
    
}
