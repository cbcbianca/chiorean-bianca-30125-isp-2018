/*Write a program that counts the number of times a particular character, such as e, appears in
a file. The character can be specified at the command line. Use data.txt as the input file.*/

package Chiorean.Bianca.g30125.l8.e2;
import java.io.*;
import java.util.*;

public class CharacterCounter {
   public static void main (String args[]) throws IOException{
	   
	   char character;
	   // 1b. Reading standard input:
	   BufferedReader stdin = new BufferedReader(
	     new InputStreamReader(System.in));
	   System.out.println("Give the character:");
	   character=stdin.readLine().charAt(0);
	   
	  int number=0; 
	  
	  //citire din fisier
	   BufferedReader in = new BufferedReader(
	          new FileReader("D:\\faculta (1)\\An 2\\Semetrul 2\\ISP\\Workspace\\Laborator8\\src\\Chiorean\\Bianca\\g30125\\l8\\e2\\words.txt"));
	          String s = new String();
	          while((s = in.readLine())!= null)
	          {
	        	  char lin[]=s.toCharArray();
	        	  for(int i=0; i<lin.length;i++)
	        	  {
	        		 if( lin[i]==character)
	        		 {
	        			number++; 
	        		 }
	        	  }
	          }
	          in.close();
	   System.out.println("The character " +character+  " appears " +number+ " times");
   }

}
