package Chiorean.Bianca.g30125.l8.e1;

class CoffeeDrinker{
    void drinkCofee(Coffee c) throws TemperatureException, ConcentrationException{
          if(c.getTemp()>60)
                throw new TemperatureException(c.getTemp(),"Cofee is to hot!");
          if(c.getConc()>50)
                throw new ConcentrationException(c.getConc(),"Cofee concentration to high!");         
          System.out.println("Drink cofee:"+c);
    }
}//.class
