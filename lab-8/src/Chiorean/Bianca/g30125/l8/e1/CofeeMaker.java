package Chiorean.Bianca.g30125.l8.e1;
//Modify cofee maker example so that CofeeMaker will throw an exception if a predefined number 
//of cofee objects are created 

class CofeeMaker {
	static int coffeeCount=0;
	int LIMIT=5;
	
    Coffee makeCofee() throws MakeException{
    	if (coffeeCount>=LIMIT)
    	{
    		throw (new MakeException("To much coffee made"));
    	}
          System.out.println("Make a coffe");
          coffeeCount++;
          int t = (int)(Math.random()*100);
          int c = (int)(Math.random()*100);
          Coffee coffee = new Coffee(t,c);
          return coffee;
    }
    

}//.class
