package Chiorean.Bianca.g30125.l8.e1;

class TemperatureException extends Exception{
    int t;
    public TemperatureException(int t,String msg) {
          super(msg);
          this.t = t;
    }

    int getTemp(){
          return t;
    }
}//.class
