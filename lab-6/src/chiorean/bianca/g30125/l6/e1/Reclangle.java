package chiorean.bianca.g30125.l6.e1;


import java.awt.*;

public class Reclangle extends Shape{

    private int length;
    private int width;

    public Reclangle(Color color, int x, int y ,int length,int width,String id,boolean filled) {
        super(color,x,y,id,filled);
        this.length = length;
        this.width=width;
    }

    public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	@Override
    public void draw(Graphics g) {
        System.out.println("Drawing a rectangel "+length+" "+getColor().toString());
        g.setColor(getColor());
        g.drawRect(getX(), getY(), getLength(), getWidth());
    }
}
