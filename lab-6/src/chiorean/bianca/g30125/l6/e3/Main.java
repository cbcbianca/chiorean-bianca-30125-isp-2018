package chiorean.bianca.g30125.l6.e3;

import java.awt.*;
public class Main {
	

	    public static void main(String[] args) {
	        DrawingBoard b1 = new DrawingBoard();
	        Shape s1 = new Circle(Color.RED, 100,70,50,"5",true);
	        b1.addShape(s1);
	        Shape s2 = new Circle(Color.GREEN,50,80,10,"4",false);
	        b1.addShape(s2);
	        Shape s3=new Rectangle(Color.BLUE,30,50,50,50,"6",false);
	        b1.addShape(s3);
	        b1.deleteShape("4");
	         
	}
}
