package chiorean.bianca.g30125.l6.e3;

import java.awt.*;

public interface Shape {
     public String getId();
     public void setId(String id);
     public int getX();
     public void setX(int x);
     public int getY();
     public void setY(int y);
     public boolean isFilled();
     public void setFilled(boolean filled);
     public Color getColor();
     public void setColor(Color color);
     public abstract void draw(Graphics g);
}
