//Program care citeste doua numere de la tastatura si afiseaza maximul dintre ele

package g30125.chiorean.bianca.l2.e1;

import java.util.Scanner;

public class exercitiu1 {
	
	public static void main (String[] args ){
		 Scanner in = new Scanner(System.in);
	       int x = in.nextInt();
	       int y = in.nextInt();
	       in.close();
	       if (x>y){
	    	   System.out.println("Maximul este:" +x);
	       }
	       else if(x==y){
	    	   System.out.println("Numerele sunt egale");
	       }
	       else System.out.println("Maximul este:" +y);
	}
    
}
