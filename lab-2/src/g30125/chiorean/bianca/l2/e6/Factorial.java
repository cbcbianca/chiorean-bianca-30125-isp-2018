//Being given an int number N, compute N! using 2 methods:

//a non recursive method
//a recursive method
package g30125.chiorean.bianca.l2.e6;
import java.util.Scanner;

public class Factorial {
	    public static void FactorialNerecursiv(int number) {
	        long result = 1;

	        for (int factor = 2; factor <= number; factor++) {
	            result *= factor;
	        }

	        System.out.println(result);
	    }
	   public static int FactorialRecursiv(int number){
		        int x=0;
		        if (number <= 1)
		            x= 1;
		        else
		           x= number*FactorialRecursiv(number - 1);
		        return x;
	 }

public static void main (String[] args){
	int number;
	System.out.println("Dati numarul:");
	Scanner in = new Scanner(System.in);
	number=in.nextInt();
	FactorialNerecursiv(number);
	FactorialRecursiv(number);
	System.out.println(FactorialRecursiv(number));
	in.close();
	
}	
}
