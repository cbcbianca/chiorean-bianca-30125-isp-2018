//Write a program which generate a vector of 10 int elements, sort them using bubble sort
//method and then display the result.

package g30125.chiorean.bianca.l2.e5;

import java.util.Random;

public class BubbleSort {
	public static void main (String[] args){
		
		int a[]=new int[10];
		int i;
		Random r = new Random();
		System.out.println("Vectorul nesortat este:");
		for(i=0;i<10;i++)
		{
			a[i]=r.nextInt(100);
			System.out.println(a[i]);
		}
		System.out.println("Vectorul sortat este:");
		int aux=0;
		for(i=0; i<10; i++) {
			for(int j=1; j < (10-i); j++){ 
	         if(a[j-1] > a[j]) {
	            aux = a[j-1];
	            a[j-1] = a[j];
	            a[j] = aux;
	     
	         }
			}   
		}
		for (i=0;i<10;i++)
		{
			System.out.println(a[i]);
		}
		
		
		
	}
}

