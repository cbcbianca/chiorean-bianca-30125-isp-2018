package g30125.chiorean.bianca.l2.e4;

//Giving a vector of N elements, display the maximum element in the vector.

import java.util.*;

public class NVector {
    
	public static void main(String[] args) {
      
	  int n,i;
	  System.out.println("n= \n");
      Scanner in = new Scanner(System.in);
      n = in.nextInt();
	  int a[]=new int[n];
      System.out.println("Dati elementele vectorului :");
      for (i=0;i<n;i++){
    	  a[i] = in.nextInt();}
      Arrays.sort(a);
      int max=a[a.length-1];
      in.close();
      System.out.println("Maximul este :" +max);
      
      
    	  
	}
}
