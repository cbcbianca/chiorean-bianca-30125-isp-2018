//Exercise PrintNumberInWord (nested-if, switch-case): Write a program called PrintNumberInWord
//which prints �ONE�, �TWO�,� , �NINE�, �OTHER� if the int variable �number� is 1, 2,� , 9,
//or other, respectively. Use (a) a �nested-if� statement; (b) a �switch-case� statement.*\

package g30125.chiorean.bianca.lab2.e2;

import java.util.*;

public class PrintNumberInWord {

	static void nestedif (int a) {
	    String number;
	    	if (a==0) number = "zero";
	    	else if (a==1) number = "one";
	    	else if (a==2) number = "two";
	        else if (a==3) number = "three";
	        else if (a==4) number = "four";
	        else if (a==5) number = "five";
	        else if (a==6) number = "six";
	        else if (a==7) number = "seven";
	        else if (a==8) number = "eight";
	        else if (a==9) number = "nine";
	        else number = "other";               
	    System.out.println(number);
	}
	
	
	static void switchcase (int b){
    String number;
    switch (b) {
    	case 0:  number = "zero";break;
        case 1:  number = "one";break;
        case 2:  number = "two";break;
        case 3:  number = "three";break;
        case 4:  number = "four";break;
        case 5:  number = "five";break;
        case 6:  number = "six";break;
        case 7:  number = "seven";break;
        case 8:  number = "eight";break;
        case 9:  number = "nine";break;
        default: number = "other";break;               
    }
    System.out.println(number);
}

	
	public static void main(String[] args) {
		int n;
		Scanner in = new Scanner(System.in);
		System.out.println("Dati n:");
		n = in.nextInt();
		in.close();
		
		System.out.println("nestedif: ");
		nestedif(n);
		
		System.out.println("switch-case");
		switchcase(n);
		
	}
}
	
