//Write a �Gues the number� game in Java. Program will generate a random number and will 
//ask user to guess it. If user guess the number program will stop. If user do not guess
//it program will display: 'Wrong answer, your number it too high' or 'Wrong answer, your
//number is too low'. Program will allow user maximum 3 retries after which will stop with 
//message 'You lost'.

package g30125.chiorean.bianca.l2.e7;

import java.util.Random;
import java.util.Scanner;
import static java.lang.System.exit;

public class GuessANumber {
    public static void main (String[] args){
    	Scanner a=new Scanner(System.in);
    	System.out.println("Guess the number!");
    	Random r=new Random();
    	int randomInt=r.nextInt(10);
    	int t=0;
    	for (int i=0;i<3;i++)
    	{
    		System.out.println("Enter a number:");
    		int userInt=a.nextInt();
    		if(userInt==randomInt)
    		{
    			System.out.println("Winner!");
    			exit(1);
    		}else
    			t++;
    		    while(userInt!=randomInt)
    		    { 
    		    	if(randomInt>userInt){
    		    		System.out.println("Your number is too low");
    		    		break;
    		    	}
    		    	else if(randomInt<userInt){
    		    		System.out.println("Your number is too high");
    		    		break;
    		    	}
    		    }
    		    if(t==3){
    		    	System.out.println("Loser!");
    		    	
    		    }
    	}
    }

}

    		
