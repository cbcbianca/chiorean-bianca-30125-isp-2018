package chiorean.bianca.g30125.l3.e3;

/*A class called Circle contains:

Two private instance variables: radius (of type double) and color (of type String), with default value of 1.0 and �red�, respectively.
Two overloaded constructors;
Two public methods: getRadius() and getArea().
Write a class which models the class Circle. Write a class TestCircle which test Circle class.
*/

public class Circle {
   private double radius;
   private String color;
   public Circle()
   {
	   radius=1.0;
	   color="red";
   }
   public Circle(double radius)
   {
	   this.radius=radius;
   }
   

public double getRadius()
   {
	  return radius; 
   }
   public double getArea()
   {
	   return(3.14*radius*radius);
   }
   @Override
   public String toString() {
	return "Circle [radius=" + radius + ", color=" + color + "]";
   }
   public static void main (String[] args)
   {   
	   Circle c=new Circle();
	   System.out.println("Radius:" + c.getRadius());
	   System.out.println("Area:"+ c.getArea());
	   System.out.println("Color:"+c.color);   
   }
}

