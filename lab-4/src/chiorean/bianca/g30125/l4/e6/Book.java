/*Modify the Book class from previous exercise to support one or more authors by changing the instance variable authors to an Author array. Reuse the Author class written earlier.

Notes:

The constructors take an array of Author (i.e., Author[]), instead of an Author instance.
The toString() method shall return �book-name by n authors�, where n is the number of authors.
A new method printAuthors() to print the names of all the authors.
You shall re-use the Author class written earlier.*/

package chiorean.bianca.g30125.l4.e6;

import chiorean.bianca.g30125.l4.e4.Author;

public class Book {
	   private String name;
       Author[] author=new Author[10];
	   private double price;
	   private int qtyStock=0;
	   
	   public Book(String name,Author[] author, double price,int qtyStock){
		   this.name=name;
		   this.author=author;
		   this.price=price;
		   this.qtyStock=qtyStock;
	   }
	   
	   public Book(String name, double price, Author[] authors) {
	        this.name = name;
	        this.price = price;
	        this.author = author;
	    }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Author[] getAuthor() {
		return author;
	}

	public void setAuthor(Author[] author) {
		this.author = author;
	}

	public double getPrice() {
		return price;
	}
	
	public String toString() {
	        return "'"+name+"' "+"by "+ author.length+" authors";
	    }
    
	 public void PrintAuthors(){
	        for ( int i=0 ; i< this.getAuthor().length ; i++)
	        {
	            System.out.println("Author["+i+"]:"+author[i]);
	        }

	    }
	public void setPrice(double price) {
		this.price = price;
	}

	public int getQtyStock() {
		return qtyStock;
	}

	public void setQtyStock(int qtyStock) {
		this.qtyStock = qtyStock;
	}

    public static void main(String[] args) {
        Author[] author=new Author[2];
        author [0] = new Author("Author1 ","author1@yahoo.ro","M");
        author [1] = new Author("Author2 ","author@gmail.ro","F");

        Book b1 = new Book("Book1 ",author,100,10);
        System.out.println(b1.toString());
        b1.PrintAuthors();
    }   
}
