package chiorean.bianca.g30125.l4.e6;
import  chiorean.bianca.g30125.l4.e4.*;

import static org.junit.Assert.*;

import org.junit.Test;

public class BookTest {

	@Test
	public void TestGetName()
	{
		Author[] author=new Author[2];
		author[0]=new Author("Author1","author1@yahoo.com","M");
		author[1]=new Author("Author2","author2@yahoo.com","F");
		Book b1=new Book("Book1",author,100,2);
		assertEquals("Book1",b1.getName());
		
	} 
	
	@Test
	public void TestGetPrice()
	{
		Author[] author=new Author[2];
		author[0]=new Author("Author1","author1@yahoo.com","M");
		author[1]=new Author("Author2","author2@yahoo.com","F");
		Book b1=new Book("Book1",author,100,2);
		assertEquals(100,b1.getPrice(),0.01);
		
	}
	@Test
	public void TestGetQtyStock()
	{
		Author[] author=new Author[2];
		author[0]=new Author("Author1","author1@yahoo.com","M");
		author[1]=new Author("Author2","author2@yahoo.com","F");
		Book b1=new Book("Book1",author,100,2);
		assertEquals(2,b1.getQtyStock());
	}
	
	@Test
	public void TestSetPrice(){
		Author[] author=new Author[2];
		author[0]=new Author("Author1","author1@yahoo.com","M");
		author[1]=new Author("Author2","author2@yahoo.com","F");
		Book b1=new Book("Book1",author,100,2);
		b1.setPrice(20);
		assertEquals(20,b1.getPrice(),0.01);
	}
	
	@Test
    public void TestSetQtyStock(){
		Author[] author=new Author[2];
		author[0]=new Author("Author1","author1@yahoo.com","M");
		author[1]=new Author("Author2","author2@yahoo.com","F");
		Book b1=new Book("Book1",author,100,2);
		b1.setQtyStock(5);
		assertEquals(5,b1.getQtyStock());
		
	}
	
	  @Test
	    public void TestPrintAllAuthors() {
	        Author[] author = new Author[2];
	        author[0] = new Author("Author1", "author1@yahoo.ro", "M");
	        author[1] = new Author("Author2", "author2@yahoo.ro", "F");
	        Book b1 = new Book("Book1",author, 50, 10);

	        assertEquals("Author1", author[0].getName());
	        assertEquals("Author2", author[1].getName());

	    }
	
	

}
