package chiorean.bianca.g30125.l4.e5;
import chiorean.bianca.g30125.l4.e4.Author;
import static org.junit.Assert.*;

import org.junit.Test;

public class BookTest {

	@Test
	public void TestToString()
	{
		Author a1 =new Author("Bruce Eckel","bruce.eckel@yahoo.com","m");
	    Book b1 = new Book( "Thinking in Java",a1,100,5);
		assertEquals("'Thinking in Java' by Bruce Eckel (m) at bruce.eckel@yahoo.com",b1.toString());
	}
	
	@Test
	public void TestGetNameBook()
	{   
		Author a1=new Author("Author1","autor email","m");
		Book b1=new Book("Book1",a1,200,30);
		assertEquals("Book1",b1.getName());
	}
	
    @Test
    public void TestGetNameAuthor()
    {
    	Author a1=new Author("Author1","author email","m");
    	assertEquals("Author1",a1.getName());
    }
    
    @Test
    public void TestGetPrice()
    {
    	Author a1=new Author("Author1","author email","m");
    	Book b1=new Book("Book1",a1,200,30);
    	assertEquals(200,b1.getPrice(),0.01);
    }
    @Test
    public void TestPriceNotNull()
    {
    	Author a1=new Author("Author1","autor email","m");
		Book b1=new Book("Book1",a1,200,30);
		assertNotNull(b1.getPrice());
    }
    
    @Test
    public void TestGetQtyStock(){
    	Author a1=new Author("Author1","autor email","m");
		Book b1=new Book("Book1",a1,200,30);
		assertNotNull(b1.getQtyStock());
    }
    
    @Test
    public void Setters(){
    	Author a1=new Author("Author1","autor email","m");
		Book b1=new Book("Book1",a1,200,30);
		b1.setPrice(100);
		b1.setQtyStock(40);
		assertEquals(100,b1.getPrice(),0.01);
		assertEquals(40,b1.getQtyStock(),0.01);
    }

}
