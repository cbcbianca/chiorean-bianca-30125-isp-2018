/*A class called Book is designed as shown in the class diagram. It contains:

Four private instance variables: name (String), author (of the class Author you have just created, assume that each book has one and only one author), price (double), and qtyInStock (int);
Two constructors:
public Book (String name, Author author, double price) {�}
public Book (String name, Author author, double price,
int qtyInStock) {�}
public methods getName(), getAuthor(), getPrice(), setPrice(), getQtyInStock(), setQtyInStock().
toString() that returns �'book-name' by author-name (gender) at email�.
(Take note that the Author's toString() method returns �author-name (gender) at email�.)*/

package chiorean.bianca.g30125.l4.e5;
import chiorean.bianca.g30125.l4.e4.Author;

public class Book  {
   private String name;
   private Author author;
   private double price;
   private int qtyStock=0;
   
   public Book(String name,Author author, double price){
	   this.name=name;
	   this.author=author;
	   this.price=price;
   }
   public Book(String name,Author author, double price, int qtyStock){
   
      this.name=name;
      this.author=author;
      this.price=price;
      this.qtyStock=qtyStock;
   }
public String getName() {
	return name;
}

public Author getAuthor() {
	return author;
}

public double getPrice() {
	return price;
}
public void setPrice(double price) {
	this.price = price;
}
public double getQtyStock() {
	return qtyStock;
}
public void setQtyStock(int qtyStock) {
	this.qtyStock = qtyStock;
}
@Override
public String toString() {
	return "'"+name+"' "+"by "+author.getName() + " (" + author.getGender() + ") " + "at " + author.getEmail();
}
public static void main(String[] args) {
    Author a1 =new Author("Bruce Eckel","bruce.eckel@yahoo.com","m");

    Book b1 = new Book( "Thinking in Java",a1,100,5);

    System.out.println(b1.toString());
}

}
