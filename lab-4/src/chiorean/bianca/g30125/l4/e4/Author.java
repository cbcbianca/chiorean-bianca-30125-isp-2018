/*A class called Author contains:

Three private instance variables: name (String), email (String), and gender (char of either 'm' or 'f');
One constructor to initialize the name, email and gender with the given values;
public Author (String name, String email, char gender) {��}
(There is no default constructor for Author, as there are no defaults for name, email and gender.)
public getters/setters: getName(), getEmail(), setEmail(), and getGender();
(There are no setters for name and gender, as these attributes cannot be changed.)
A toString() method that returns �author-name (gender) at email�, e.g., �My Name (m) at myemail@somewhere.com�.
*/
package chiorean.bianca.g30125.l4.e4;

public class Author {
  
	public String name,email;
    public String gender;
  
     public Author(String name, String email, String gender){
	  this.name=name;
	  this.email=email;
	  this.gender=gender;
	 }

	public String getName() {
		return name;
	}

	public String getGender() {
		return gender;
	}

	public String getEmail() {
		return email;
	}

	@Override
	public String toString() {
		return  name + "(" + gender + ")" + email;
	}
	
	public static void main(String[] args) {
		Author a=new Author("Bianca","cbc@yahoo.com","f");
		System.out.println(a.toString());
		
	}
 }

