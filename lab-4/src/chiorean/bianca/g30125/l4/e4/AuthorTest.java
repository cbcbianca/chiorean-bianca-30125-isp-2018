package chiorean.bianca.g30125.l4.e4;

import static org.junit.Assert.*;

import org.junit.Test;

public class AuthorTest {

	@Test
	public void toStringtest() {
		Author a=new Author("Chiorean Bianca","cbc@yahoo.com","f");
		assertEquals("Chiorean Bianca(f)cbc@yahoo.com",a.toString());
	}
}
