package chiorean.bianca.g30125.l4.e8;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class SquareTest {
    @Test
    public void testGetSide(){
        Square square=new Square(2);
        assertEquals(2,square.getSide(),0.01);
    }

    @Test
    public void testSetSide(){
        Square square=new Square(2);
        square.setSide(100);
        assertEquals(100,square.getSide(),0.01);
    }

    @Test
    public void testSetWidth(){
        Square square=new Square(2);
        square.setWidth(2000);
        assertEquals(2000, square.getWidth(),0.01);
    }

}

