package chiorean.bianca.g30125.l4.e8;
/*Write a superclass called Shape (as shown in the class diagram), which contains:

Two instance variables color (String) and filled (boolean).
Two constructors: a no-arg (no-argument) constructor that initializes the color to �green� and filled to true, and a constructor that initializes the color and filled to the given values.
Getter and setter for all the instance variables. By convention, the getter for a boolean variable xxx is called isXXX() (instead of getXxx() for all the other types).
A toString() method that returns �A Shape with color of xxx and filled/Not filled�.*/

public class Shape {
  protected String color;
  protected boolean filled;
  public Shape()
  {
	  this.color="red";
	  this.filled=true;
  }
  public Shape(String color,boolean filled)
  {
	  this.color=color;
	  this.filled=filled;
  }
public String getColor() {
	return color;
}
public void setColor(String color) {
	this.color = color;
}
public boolean isFilled() {
	return filled;
}
public void setFilled(boolean filled) {
	this.filled = filled;
}
@Override
public String toString() {
	return " A Shape with color=" + color + " and " + filled  ;
}

public static void main(String[] args) {
    Shape shape= new Shape("black",true);
    System.out.println(shape.toString());
}
  
}
