package chiorean.bianca.g30125.l4.e8;

import org.junit.*;

import static org.junit.Assert.assertEquals;

public class CircleTest {
    @Test
    public void TestGetRadius(){
        Circle circle = new Circle(1 );
        assertEquals(1, circle.getRadius(),0.01);
    }
    @Test
    public void TestGetArea(){
        Circle circle = new Circle(3);
        assertEquals(28.274, circle.getArea(),0.01);
    }
    @Test
    public void TestGetPerimeter(){
        Circle circle = new Circle(3);
        assertEquals(18.849, circle.getPerimeter(),0.01);
    }
    @Test
    public void TestSetRadius(){
        Circle circle = new Circle(3 );
        circle.setRadius(10);
        assertEquals(10, circle.getRadius(),0.01);
    }


}

