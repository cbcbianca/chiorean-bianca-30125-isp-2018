package chiorean.bianca.g30125.l4.e8;
import static java.lang.Boolean.TRUE;
public class Square extends Rectangle{
   
    public Square()
    {
    	super.width=1.0;
    	super.length=1.0;
    }
    public Square(double side)
    {
    	super.width=side;
    	super.length=side;
    }
    public Square(double side,boolean filled , String color)
    {
    	super.width=side;
    	super.length=side;
    	super.filled=filled;
    	super.color=color;
    }
    public double getSide(){
        return length; //same with width
    }

    public void setSide(double side){
        super.length=side;
        super.width=side;
    }
    
    public String toString(){
        return "A Square with side= "+length+" , which is a subclass of "+super.toString();
    }
    

    public static void main(String[] args) {
        Square square = new Square(4,TRUE,"red");
        System.out.println(square.toString());
    }
}
