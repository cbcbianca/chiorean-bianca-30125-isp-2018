package chiorean.bianca.g30125.l4.e7;
import chiorean.bianca.g30125.l3.e3.Circle;
import static org.junit.Assert.*;

import org.junit.Test;

public class TestCylinder {

	@Test
	public void TestGetHeight(){
		Cylinder c1=new Cylinder(3,4);
		assertEquals(3,c1.getHeight(),0.01);
	}
	
	@Test
	public void TestGetVolume()
	{
		Circle c2=new Circle();
		Cylinder c1=new Cylinder(1,c2.getRadius());
        assertEquals(3.14,c1.getVolume(),0.01);
	}
	
	

}
