/*In this exercise, a subclass called Cylinder is derived from the superclass Circle as shown in the class diagram.

Notes:

 Circle class from the previous exercise;
Use 'super' keyword to call constructors from the base class;
Observe that getArea() method from derived class returns a wrong value because it uses the formula for circle;
Overwrite getArea() method in order to correctly calculate the area for the derived class;*/

package chiorean.bianca.g30125.l4.e7;
import chiorean.bianca.g30125.l3.e3.Circle;

public class Cylinder extends Circle{
	
	private double height;
	public Cylinder()
	{
		super();
		this.height=1.0;
	}
	public Cylinder(double radius){
		super(radius);
	}
	public Cylinder(double height,double radius)
	{
		super(radius);
		this.height=height;	
	}
	public double getHeight() {
		return height;
	}
	
	public double getVolume(){
        return super.getArea()*height;
    }
	
	public static void main(String[] args)
	{
		Cylinder cylinder = new Cylinder(10,15);
        System.out.println("Area:"+cylinder.getArea());
        System.out.println("Volume: "+cylinder.getVolume());

	}
	
	
}
