package g30125.chiorean.bianca.l3.e5;

import becker.robots.*;


public class WalkingAroundTheWalls {
	public static void main(String[] args){
	    City A = new City();
	    Wall blockAve0 = new Wall(A, 1, 1, Direction.NORTH);
	    Wall blockAve1 = new Wall(A, 1, 1, Direction.WEST);
	    Wall blockAve2 = new Wall(A, 1, 2, Direction.NORTH);
	    Wall blockAve3 = new Wall(A, 1, 2, Direction.EAST);
	    Wall blockAve4 = new Wall(A, 2, 2, Direction.EAST);
	    Wall blockAve5 = new Wall(A, 2, 2, Direction.SOUTH);
	    Wall blockAve6 = new Wall(A, 2, 1, Direction.SOUTH);
	    Wall blockAve7 = new Wall(A, 2, 1, Direction.WEST);
	    Robot clock = new Robot(A, 0, 2, Direction.WEST);
	    for (int i=0;i<2;i++){
		    clock.turnLeft();}
	    clock.move();
	    for (int i=0;i<3;i++){ //go around
	    clock.turnLeft();
	    clock.turnLeft();
	    clock.turnLeft();
	    clock.move();
	    clock.move();
	    clock.move();
	    }
       for (int i=0;i<3;i++){ //turn right
	    clock.turnLeft();}
       for (int i=0;i<2;i++){ //go right
	    clock.move();}
       for (int i=0;i<2;i++){ //turn around.initial position
	    clock.turnLeft();}
	 }
}
