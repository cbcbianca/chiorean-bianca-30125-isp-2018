//Write a program that creates a robot at (1, 1) that moves north five times, turns around, 
//and returns to its starting point.

package g30125.chiorean.bianca.l3.e3;

import becker.robots.*;


public class North5times {
	public static void main(String[] args){
		City Winterfell = new City();
		Robot Snow=new Robot(Winterfell,1,1, Direction.NORTH );
		
		
		int i;
		for (i=0;i<5;i++)
		{
			Snow.move();
		}
		Snow.turnLeft();
		Snow.turnLeft();
		int j=0;
		for (j=0;j<5;j++)
		{
			Snow.move();
		}
		
	}
}
