package g30125.chiorean.bianca.l3.e6;


import static org.junit.Assert.*;


import org.junit.Before;
import org.junit.Test;

public class TestMyUnit {
	private MyPoint mp;
	
	@Before
	public void setUp(){
		mp=new MyPoint(2,2);
	}
	
	@Test
	public void testGetDistance(){
		Double distanta =mp.getDistance(2, 2);
		assertEquals(Double.valueOf(0),distanta);
	}
	
	@Test
	public void testGetDistanceObject(){
		MyPoint other=new MyPoint(2,2);
		Double distanta=mp.getDistance(other);
		assertEquals(Double.valueOf(0),distanta);
	}
	
	@Test
	public void testToString(){
		//String rez=mp.toString();
		//String rez2=new String("(2,2)");
		//assertEquals(rez,rez2);
		assertEquals(mp.toString(),"(2,2)");
	}
	
	@Test
	public void testSetXY(){
		mp.setXY(5, 6);
		assertEquals(mp.getX(),5);
		assertEquals(mp.getY(),6);
	}
	
	@Test
	public void testGetters(){
		assertEquals(mp.getX(),2);
		assertEquals(mp.getY(),2);
	}
	
	@Test
	public void testSetters(){
		mp.setX(7);
		mp.setY(9);
		assertEquals(mp.getX(),7);
		assertEquals(mp.getY(),9);
	}
	
	
}
