/*A class called MyPoint, which models a 2D point with x and y coordinates contains:

Two instance variables x (int) and y (int).
A �no-argument� (or �no-arg�) constructor that construct a point at (0, 0).
A constructor that constructs a point with the given x and y coordinates.
Getter and setter for the instance variables x and y.
A method setXY() to set both x and y.
A toString() method that returns a string description of the instance in the format �(x, y)�.
A method called distance(int x, int y) that returns the distance from this point to another point at the given (x, y) coordinates.
An overloaded distance(MyPoint another) that returns the distance from this point to the given MyPoint instance another.
Write the code for the class MyPoint. Also write a test class (called TestMyPoint) to test all the methods defined in the class.*/



package g30125.chiorean.bianca.l3.e6;
import java.lang.Math;


public class MyPoint {
	//Scanner scan=new Scanner(System.in);
    private int x;
    private int y;
    //private static int j;
    //private final int u=766;
    
    public MyPoint(){ //no-arguments constructor
    	 this.x=0;
         this.y=0;
    }
    public MyPoint(int x,int y){ //A constructor that constructs a point with the given x and y coordinates.
    	this.x=x;
    	this.y=y;
    }
    
    //Getter and setter for the instance variables x and y.   Source-> Generate
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	
	//A method setXY() to set both x and y.
	public void setXY(int x,int y)
	{
		this.x=x;
		this.y=y;
	}
	
	//A method called distance(int x, int y) that returns the distance from this point to another point at the given (x, y) coordinates.
	public double getDistance(int x,int y){
		return Math.sqrt(Math.pow((this.x-x),2)+Math.pow((this.y-y),2));
	}
	
	public boolean equals(MyPoint other) {
		return (this.x==other.x && this.y==other.y);
	}
	//An overloaded distance(MyPoint another) that returns the distance from this point to the given MyPoint instance another.
	public double getDistance(MyPoint other){
		return Math.sqrt(Math.pow((this.x-other.x),2)+Math.pow((this.y-other.y),2));
	}
	
	//A toString() method that returns a string description of the instance in the format �(x, y)�.
	
	//Source->Generate
	@Override
	public String toString() {
		return "(" + x + "," + y + ")";
	}

	

	
	
   
    
   
    
    
    
    
}
