//Every morning karel is awakened when the newspaper, represented by a Thing,
//is thrown on the front porch of its house. Instruct karel to retrieve the newspaper
//and return to �bed.� The initial situation is as shown in Figure 1-29; in the
//final situation, karel is on its original intersection, facing its original direction,
//with the newspaper.
package g30125.chiorean.bianca.l3.e4;

import becker.robots.*;
import javax.swing.JOptionPane;

public class Karel {
	public static void main(String[] args){
    City Cluj = new City();
    Thing ziar = new Thing(Cluj, 2, 2);
    int i=0;
    Wall blockAve0 = new Wall(Cluj, 1, 2, Direction.SOUTH);
    Wall blockAve1 = new Wall(Cluj, 1, 2, Direction.EAST);
    Wall blockAve2 = new Wall(Cluj, 1, 2, Direction.NORTH);
    Wall blockAve3 = new Wall(Cluj, 1, 1, Direction.NORTH);
    Wall blockAve4 = new Wall(Cluj, 1, 1, Direction.WEST);
    Wall blockAve5 = new Wall(Cluj, 2, 1, Direction.WEST);
    Wall blockAve6 = new Wall(Cluj, 2, 1, Direction.SOUTH);
    Robot karel = new Robot(Cluj, 1, 2, Direction.SOUTH);
    for(i=0;i<3;i++){
		karel.turnLeft();}
    for(i=0;i<2;i++){
    	karel.move();
        karel.turnLeft();}
    karel.move();
    karel.pickThing();
    for(i=0;i<2;i++){
		karel.turnLeft();}
    karel.move();
    for(i=0;i<3;i++){
		karel.turnLeft();}
    karel.move();
    for(i=0;i<3;i++){
		karel.turnLeft();}
    karel.move();
 }
}
	