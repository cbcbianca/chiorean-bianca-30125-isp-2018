package Chiorean.Bianca.g30125.l10.e3;

public class Counter extends Thread {
     
	String n;
	Thread t;
	
	Counter(String n, Thread t)
	{
		this.n=n;
		this.t=t;
	}
	
	public void run()
	{
		System.out.println("Firul"+n+" a intrat in metoda run()");
		try
		{
			if(t!=null)
			{
			t.join();
			for(int i=100;i<201;i++)
			{
				try{ System.out.println (n+" este pe pozitia "+i);
				Thread.sleep(100);
				}catch(Exception e){e.printStackTrace();}
			
			}
		}
			else{

                for(int i=0; i<100; i++){
                    try{
                        System.out.println(n+ " este la pozitia : " +(i));
                        Thread.sleep(100);
                    } catch(Exception e){e.printStackTrace();}
                }

            }

        }
        catch(Exception e){e.printStackTrace();}

    }		
	
public static void main(String[] args)
{
    Counter w1 = new Counter("Proces 1",null);
    Counter w2 = new Counter("Proces 2",w1);
    w1.start();
    w2.start();
}}	

