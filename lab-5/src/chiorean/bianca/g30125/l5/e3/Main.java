package chiorean.bianca.g30125.l5.e3;

public class Main {
  public static void main(String[] args) throws InterruptedException
  {
	TemperatureSensor s1=new TemperatureSensor();
	LightSensor s2=new LightSensor();
	
	s1.getLocation(); 
	s2.getLocation();
	
	System.out.println(s1.readValue());
	System.out.println(s2.readValue());
	
	System.out.println("\n Testing the controller:");
	
	for (int i=0;i<20;i++)
	{
		Controller c=new Controller(s1,s2);
		c.control();
		Thread.sleep(1000);
	}
  }
}
