package chiorean.bianca.g30125.l5.e3;

import java.util.Scanner;

public abstract class Sensor {
    String location;
    
    abstract int readValue();
    
    public String getLocation(){
    	System.out.println("Location:");
    	Scanner in=new Scanner(System.in);
    	location=in.nextLine();
    	return location;
    }
}
