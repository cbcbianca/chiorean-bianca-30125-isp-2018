package chiorean.bianca.g30125.l5.e1;

public class Circle extends Shape {
	protected double radius;

	   public Circle(){

	   }

	   public Circle(double radius) {
	       this.radius = radius;
	   }

	   public Circle(String color, boolean filled, double radius) {
	       super(color, filled);
	       this.radius = radius;
	   }

	   public double getRadius() {
	       return radius;
	   }

	   public void setRadius(double radius) {
	       this.radius = radius;
	   }

	   public double getArea() {
	       return 3.14*radius*radius ; 
	   }

	   public double getPerimeter() {
	       return 2*3.14*radius; //replace with formula for get perimeter
	   }

	   @Override
	   public String toString() {
	       return "Circle{" +
	               "color='" + color + '\'' +
	               ", filled=" + filled +
	               ", radius=" + radius +
	               '}';
	   }
}
